package com.storeapi.domain.product;

import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;

import com.storeapi.domain.product.exception.ProductAlreadyExistsException;
import com.storeapi.infrastructure.ProductNotFoundException;
import com.storeapi.service.ProductService;
import com.storeapi.transfer.ProductAssembler;
import com.storeapi.transfer.ProductDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    private final static String A_PRODUCT_NAME = "Product";
    private final static String A_QUANTITY = "1";

    @Mock
    private Product product;
    @Mock
    private ProductDto productDto;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ProductAssembler productAssembler;
    @Mock
    private ProductService productService;

    @Before
    public void setUp()
        throws Exception {
        productService = new ProductService(productRepository, productAssembler);
    }

    @Test
    public void givenNewProduct_whenAddProduct_thenProductIsAdded() throws ProductAlreadyExistsException {
        willReturn(product).given(productAssembler).create(productDto);

        productService.addProduct(productDto);

        verify(productRepository).save(product);
    }

    @Test
    public void givenAProduct_whenBuyingProduct_thenProductIsUpdated() throws ProductAlreadyExistsException, TooManyProductsToRemoveException, ProductNotFoundException {
        productService.buyProduct(A_PRODUCT_NAME, A_QUANTITY);

        verify(productRepository).save(product);

    }

}