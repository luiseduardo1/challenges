package com.storeapi.infrastructure.product;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.BDDMockito.given;

import com.storeapi.domain.product.Product;
import com.storeapi.domain.product.exception.ProductAlreadyExistsException;
import com.storeapi.infrastructure.ProductRepositoryInMemory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class ProductRepositoryInMemoryTest {

    private static final String PRODUCT_NAME = "Product name";

    @Mock
    private Product product;

    private ProductRepositoryInMemory productRepositoryInMemory;

    @Before
    public void setUp() {
        productRepositoryInMemory = new ProductRepositoryInMemory();
        given(product.getProductName()).willReturn(PRODUCT_NAME);
    }

    @Test
    public void givenProduct_whenSave_thenProductHasSameParameters() throws ProductAlreadyExistsException {
        productRepositoryInMemory.save(product);

        Product savedProduct = productRepositoryInMemory.findByName(product.getProductName());

        assertEquals(product, savedProduct);
    }

}