package com.storeapi.api.resource;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

import com.storeapi.api.ProductResource;
import com.storeapi.api.ProductResourceImpl;
import com.storeapi.domain.product.TooManyProductsToRemoveException;
import com.storeapi.domain.product.exception.ProductAlreadyExistsException;
import com.storeapi.infrastructure.ProductNotFoundException;
import com.storeapi.service.ProductService;
import com.storeapi.transfer.ProductDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

@RunWith(MockitoJUnitRunner.class)
public class ProductResourceImplTest {

    @Mock
    private ProductService productService;
    @Mock
    private ProductDto productDto;

    private ProductResource productResource;

    private final static String A_PRODUCT_NAME = "Product";
    private final static String A_QUANTITY = "1";

    @Before
    public void setUp()
        throws Exception {
        productResource = new ProductResourceImpl(productService);
    }

    @Test
    public void whenAddAProduct_thenDelegateToService() throws ProductAlreadyExistsException {
        productResource.addProduct(productDto);
        verify(productService).addProduct(productDto);
    }

    @Test
    public void whenBuyAProduct_thenDelegateToService() throws ProductAlreadyExistsException, TooManyProductsToRemoveException, ProductNotFoundException {
        productResource.buyProduct(A_PRODUCT_NAME, A_QUANTITY);
        verify(productService).buyProduct(A_PRODUCT_NAME, A_QUANTITY);
    }

    @Test
    public void giverAResource_whenAddAProduct_thenReturnsCreated() {
        Response response = productResource.addProduct(productDto);
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    }

    @Test
    public void giverAResource_whenBuyAProduct_thenReturnsCreated() {
        Response response = productResource.buyProduct(A_PRODUCT_NAME, A_QUANTITY);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

}