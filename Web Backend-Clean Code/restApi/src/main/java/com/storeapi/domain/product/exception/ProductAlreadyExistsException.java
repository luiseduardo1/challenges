package com.storeapi.domain.product.exception;

public class ProductAlreadyExistsException extends Throwable {

    public ProductAlreadyExistsException(String message) {
        super(message);
    }
}
