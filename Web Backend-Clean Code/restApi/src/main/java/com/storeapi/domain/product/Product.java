package com.storeapi.domain.product;

public class Product {

    private int quantity;
    private double price;
    private String productName;


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void removeQuantity(int quantityToRemove) throws TooManyProductsToRemoveException {
        if (quantityToRemove > quantity) {
            throw new TooManyProductsToRemoveException();
        } else {
            quantity = quantity - quantityToRemove;
        }
    }
}
