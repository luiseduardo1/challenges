package com.storeapi.domain.product;

import com.storeapi.domain.product.exception.ProductAlreadyExistsException;
import com.storeapi.infrastructure.ProductNotFoundException;

public interface ProductRepository {

    Product findByName(String productName);

    void update(Product product) throws ProductNotFoundException;

    void save(Product product) throws ProductAlreadyExistsException;

    void remove(String productName);
}
