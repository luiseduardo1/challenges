package com.storeapi.transfer;

import com.storeapi.domain.product.Product;

public class ProductAssembler {

    public Product create(ProductDto productDto) {
        Product product = new Product();
        product.setProductName(productDto.getProductName());
        product.setQuantity(productDto.getQuantity());
        product.setPrice(productDto.getPrice());
        return product;
    }
}
