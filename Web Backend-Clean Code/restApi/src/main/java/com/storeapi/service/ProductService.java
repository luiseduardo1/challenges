package com.storeapi.service;

import static java.lang.Integer.parseInt;

import com.storeapi.domain.product.Product;
import com.storeapi.domain.product.ProductRepository;
import com.storeapi.domain.product.TooManyProductsToRemoveException;
import com.storeapi.domain.product.exception.ProductAlreadyExistsException;
import com.storeapi.infrastructure.ProductNotFoundException;
import com.storeapi.transfer.ProductAssembler;
import com.storeapi.transfer.ProductDto;

public class ProductService {

    private ProductRepository productRepository;
    private ProductAssembler productAssembler;

    public ProductService(ProductRepository productRepository, ProductAssembler productAssembler) {
        this.productRepository = productRepository;
        this.productAssembler = productAssembler;
    }

    public void addProduct(ProductDto productDto) throws ProductAlreadyExistsException {
        Product product = productAssembler.create(productDto);
        productRepository.save(product);
    }

    public void buyProduct(String productName, String quantity) throws ProductAlreadyExistsException,
        TooManyProductsToRemoveException, ProductNotFoundException {
        int quantityToBuy = parseInt(quantity);
        Product product = productRepository.findByName(productName);
        product.removeQuantity(quantityToBuy);
        productRepository.update(product);

    }
}
