package com.storeapi.infrastructure;

import com.storeapi.domain.product.Product;
import com.storeapi.domain.product.ProductRepository;
import com.storeapi.domain.product.exception.ProductAlreadyExistsException;

import java.util.HashMap;
import java.util.Map;

public class ProductRepositoryInMemory implements ProductRepository {

    private Map<String, Product> products = new HashMap<>();

    @Override
    public Product findByName(String productName) {
        return products.get(productName);
    }

    @Override
    public void update(Product product) throws ProductNotFoundException {
        Product foundProduct = products.get(product.getProductName());
        if (foundProduct != null) {
            products.put(product.getProductName(), product);
        } else {
            throw new ProductNotFoundException("product not found, cannot be updated");
        }
    }

    @Override
    public void save(Product product) throws ProductAlreadyExistsException {
        String productName = product.getProductName();
        if (products.containsKey(productName)) {
            throw new ProductAlreadyExistsException(
                String.format("Product with product name %s already exists.", productName)
            );
        }
        products.put(productName, product);
    }

    @Override
    public void remove(String productName) {
        products.remove(products.remove(productName));
    }
}
