package com.storeapi.infrastructure;

public class ProductNotFoundException extends Throwable {

    public ProductNotFoundException(String s) {
        super(s);
    }
}
