package com.storeapi;

import com.storeapi.api.ProductResource;
import com.storeapi.api.ProductResourceImpl;
import com.storeapi.domain.product.ProductRepository;
import com.storeapi.http.CORSResponseFilter;
import com.storeapi.infrastructure.ProductRepositoryInMemory;
import com.storeapi.service.ProductService;
import com.storeapi.transfer.ProductAssembler;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class StoreApiMain {

    public static void main(String[] args) throws Exception {

        ProductResource productResource = createProductResource();

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/api/");
        ResourceConfig resourceConfig = ResourceConfig.forApplication(new Application() {
            @Override
            public Set<Object> getSingletons() {
                HashSet<Object> resources = new HashSet<>();
                resources.add(productResource);
                return resources;
            }
        });

        resourceConfig.register(CORSResponseFilter.class);

        ServletContainer servletContainer = new ServletContainer(resourceConfig);
        ServletHolder servletHolder = new ServletHolder(servletContainer);
        context.addServlet(servletHolder, "/*");

        // Setup http server
        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[]{context});
        Server server = new Server(8080);
        server.setHandler(contexts);

        try {
            server.start();
            server.join();
        } finally {
            server.destroy();
        }
    }

    private static ProductResource createProductResource() {
        ProductRepository productRepository = new ProductRepositoryInMemory();

        ProductAssembler productAssembler = new ProductAssembler();
        ProductService productService = new ProductService(productRepository, productAssembler);
        return new ProductResourceImpl(productService);
    }
}
