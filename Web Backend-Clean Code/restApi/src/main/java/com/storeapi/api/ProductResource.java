package com.storeapi.api;

import com.storeapi.transfer.ProductDto;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/add")
public interface ProductResource {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    Response addProduct(ProductDto productDto);

    @POST
    @Path("/buy/{productName}?quantity={quantity}")
    Response buyProduct(@PathParam("productName") String productName, @PathParam("quantity") String quantity);

}
