package com.storeapi.api;

import com.storeapi.domain.product.TooManyProductsToRemoveException;
import com.storeapi.domain.product.exception.ProductAlreadyExistsException;
import com.storeapi.infrastructure.ProductNotFoundException;
import com.storeapi.service.ProductService;
import com.storeapi.transfer.ProductDto;

import javax.ws.rs.core.Response;

public class ProductResourceImpl implements ProductResource {

    private final ProductService productService;

    public ProductResourceImpl(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Response addProduct(ProductDto productDto) {
        try {
            productService.addProduct(productDto);
            return Response.status(Response.Status.CREATED).build();
        } catch (ProductAlreadyExistsException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @Override
    public Response buyProduct(String productName, String quantity) {
        try {
            productService.buyProduct(productName, quantity);
            return Response.ok().build();
        } catch (ProductAlreadyExistsException | ProductNotFoundException | TooManyProductsToRemoveException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
